import { Component, AfterViewInit } from '@angular/core';

import * as L from 'leaflet';

@Component({

  selector: 'app-map',

  templateUrl: './map.component.html',

  styleUrls: ['./map.component.scss']

})

export class MapComponent implements AfterViewInit {

  private map: L.Map | L.LayerGroup<any> | undefined;

  //ИНИЦИАЛИЗИРУЕМ КАРТУ

  private initMap(): void {

    this.map = L.map('map', {

      center: [ 43.1056200, 131.8735300 ],

      zoom: 13
      
    });

//ДОБАВЛЯЕМ МАРКЕРЫ
    var marker_off = L.icon({
      iconUrl: '/icons/Tablo_Off.png',
      iconSize: [40, 40]
    });

    var marker_on = L.icon({
      iconUrl: '/icons/Tablo_ON.png',
      iconSize: [40, 40]
    });


    var marker_1 = new L.Marker([43.119318, 131.891129], {icon: marker_off}).addTo(this.map);


    var marker_2 = new L.Marker([43.115291, 131.881962], {icon: marker_on}).addTo(this.map);


    //ПОДКЛЮЧАЕМ КАРТУ OPENSTREETMAP
    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

      maxZoom: 18,

      minZoom: 3,

      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'

    });

    tiles.addTo(this.map);
    marker_1.addTo(this.map);
    marker_2.addTo(this.map);

  }

  constructor() { }

  ngAfterViewInit(): void {

    this.initMap();

  }

}

